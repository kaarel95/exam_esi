package com.example.demo.sales.application.service;

import com.example.demo.common.application.service.BusinessPeriodValidator;
import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.model.ReservationType;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.demo.sales.domain.POStatus.PRERESERVED;

@Service
public class SalesService {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    PreReservedPORepository preReservedPORepository;

    @Autowired
    PurchaseOrderRepository poRepository;

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    public PurchaseOrderDTO findPO(Long id) {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO createPO(PurchaseOrderDTO poDTO) throws Exception {

        BusinessPeriod period = BusinessPeriod.of(
                         poDTO.getRentalPeriod().getStartDate(),
                         poDTO.getRentalPeriod().getEndDate());

        DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new Exception("Invalid PO Period");

        if(poDTO.getPlant() == null)
            throw new Exception("Invalid Input Plant");

        PlantInventoryEntry plant = plantInventoryEntryRepository.findById(poDTO.getPlant().get_id()).orElse(null);

        if(plant == null)
            throw new Exception("Plant NOT Found");

        PurchaseOrder po = PurchaseOrder.of(plant, period);
        poRepository.save(po);

        List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(
                po.getPlant().getId(),
                po.getRentalPeriod().getStartDate(),
                po.getRentalPeriod().getEndDate());

        if(availableItems.size() == 0) {
            po.setStatus(POStatus.REJECTED);
            throw new Exception("No available items");
        }

        PlantReservation reservation = new PlantReservation();
        reservation.setSchedule(po.getRentalPeriod());
        reservation.setPlant(availableItems.get(0));

        plantReservationRepository.save(reservation);
        po.getReservations().add(reservation);

        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);

    }

    public PurchaseOrderDTO acceptPO(Long id) throws Exception {
        PurchaseOrder po = getPO(id);
        po.setStatus(POStatus.OPEN);
        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO rejectPO(Long id) throws Exception {
        PurchaseOrder po = getPO(id);
        while (!po.getReservations().isEmpty()) {
            plantReservationRepository.delete(po.getReservations().remove(0));
        }
        po.setStatus(POStatus.CLOSED);
        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    private PurchaseOrder getPO(Long id) throws Exception {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        if(po == null)
            throw new Exception("PO Not Found");
        if(po.getStatus() != POStatus.PENDING)
            throw new Exception("PO cannot be accepted/rejected due to it is not Pending");
        return po;
    }

    public PlantReservation createTentativeReservation(Long id, LocalDate startDate, LocalDate endDate) throws Exception {
        if (!startDate.minusDays(6).isBefore(LocalDate.now())) {
            throw new Exception("Tentative reservation must be created 5 days before start");
        }
        PlantInventoryItem plantInventoryItem = plantInventoryItemRepository.findById(id).orElse(null);
        if(plantInventoryItem == null){
            throw new Exception("this plant intentory item does not exist");
        }
        List<PlantReservation> collect = plantReservationRepository.findAll().stream().filter(plantReservation -> plantReservation.getPlant().getId() == id).collect(Collectors.toList());
        for (PlantReservation plantReservation : collect) {
            LocalDate reservationStart = plantReservation.getSchedule().getStartDate();
            LocalDate reservationEnd = plantReservation.getSchedule().getEndDate();
            // did not have time to implement
          //  if (plantAlreadyReserved(startDate, endDate, reservationStart, reservationEnd)) {
           //     throw new Exception("Plant already reserved for that time period");
           // }
        }
        PlantReservation reservation = new PlantReservation();
        reservation.setSchedule(BusinessPeriod.of(startDate, endDate));
        reservation.setPlant(plantInventoryItem);
        reservation.setType(ReservationType.TENTATIVE);
        PlantReservation save = plantReservationRepository.save(reservation);
        return save;
    }

    private boolean plantAlreadyReserved(LocalDate startDate, LocalDate endDate, LocalDate reservationStart, LocalDate reservationEnd) {
        return false;
    }

    public PreReservedPO createPreReservedPO(Long id) throws Exception {
        PlantReservation plantReservation = plantReservationRepository.findById(id).orElse(null);
        if(plantReservation == null ) {
            throw new Exception("Plant reservation does not exist");
        }
        if( plantReservation.getType() != ReservationType.TENTATIVE) {
            throw new Exception("cant create PO for this reservation");
        }
        PreReservedPO preReservedPO = new PreReservedPO();
        preReservedPO.addReservation(plantReservation);
        preReservedPO.setStatus(PRERESERVED);
        PreReservedPO save = preReservedPORepository.save(preReservedPO);
        return save;
    }
}
