package com.example.demo.sales.domain;


import com.example.demo.inventory.domain.model.PlantReservation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class PreReservedPO {
    @Id
    @GeneratedValue
    Long id;
    @OneToMany
    List<PlantReservation> reservations;
    @Enumerated(EnumType.STRING)
    POStatus status;

    public void addReservation(PlantReservation reservation) {
        if(reservations == null ){
            reservations = Arrays.asList(reservation);
        }else {
            reservations.add(reservation);
        }
    }
}
