package com.example.demo.inventory.domain.model;

public enum ReservationType {
    TENTATIVE, ORDINARY
}
